from tkinter import *

class Bruger:
    def __init__(self, rolle, navn):
        self.rolle = rolle
        self.navn = navn
def update():
    global oprettet
    oprettet = True
    label1.config(text=bruger.rolle)
    label2.config(text=bruger.navn)


def nybruger():
    global bruger
    bruger_rolle = valg.get()
    bruger_navn = navn_entry.get()
    bruger = Bruger(rolle=bruger_rolle, navn=bruger_navn)
    opret.destroy()
    update()


def opret():
    global navn_entry, valg, opret
    opret = Toplevel()
    opret.title("Opret")

    valg = StringVar()
    rolle_menu= OptionMenu(opret, valg, "Administrator", "Superbruger", "Bruger", "Tester")
    rolle_menu.grid(column=2, row=2)

    navn_entry = Entry(opret)
    navn_entry.grid(column=2, row=3, padx=15)

    rolle_label = Label(opret, text="Rolle")
    rolle_label.grid(column=1, row=2)

    navn_label = Label(opret, text="Navn")
    navn_label.grid(column=1, row=3, pady=5)

    gem_knap = Button(opret, text="Gem", command=nybruger)
    gem_knap.grid(column=2, row=1, pady=10)

def vis():
    if oprettet == True:
        root.destroy()
        vis_root = Tk()
        vis_root.title("Vis")
        Label(vis_root, text=bruger.rolle).grid(column=2, row=1, padx=5, pady=5)
        Label(vis_root, text=bruger.navn).grid(column=2, row=2, padx=5, pady=5)

        rolle_label = Label(vis_root, text="Rolle")
        rolle_label.grid(column=1, row=1, padx=5)

        navn_label = Label(vis_root, text="Navn")
        navn_label.grid(column=1, row=2, padx=5)
def main():
    global label1, label2, root, oprettet
    oprettet = False

    root = Tk()
    root.title("Logind")

    rolle_label = Label(root, text="Rolle")
    rolle_label.grid(column=1, row=2, padx=15)

    navn_label = Label(root, text="Navn")
    navn_label.grid(column=1, row=3)

    label1 = Label(root, text="label1")
    label1.grid(column=2, row=2)
    label2 = Label(root, text="label2")
    label2.grid(column=2, row=3)

    opret_knap = Button(root, text="Opret", command=opret)
    opret_knap.grid(column=3, row=4, pady=15, padx=5)

    logind_knap = Button(root, text="Logind", width=15, command=vis)
    logind_knap.grid(column=2, row=1, pady=15)

    root.mainloop()

main()