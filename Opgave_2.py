from tkinter import *
class Vare:
    def __init__(self, type_vare, navn, pris, antal):
        self.type = type_vare
        self.navn = navn
        self.pris = pris
        self.antal = antal

def tilføj():
    ny_vare = Vare(type_vare=valg.get(), navn=navn_entry.get(), pris=pris_entry.get(), antal=antal_entry.get())
    indkøb.append(ny_vare)
    varer.append(str(ny_vare.navn) + " x " +str(ny_vare.antal))
    seddel_listbox.insert(END, varer[len(varer)-1])
    pris_listbox.insert(END, float(indkøb[len(indkøb) - 1].pris)*float(indkøb[len(indkøb) - 1].antal))
def slet():
    varer.remove(varer[seddel_listbox.curselection()[0]])
    indkøb.remove(indkøb[seddel_listbox.curselection()[0]])
    pris_listbox.delete(seddel_listbox.curselection()[0])
    seddel_listbox.delete(seddel_listbox.curselection()[0])

def sorter():
    global varer, indkøb
    sorteret = []
    for i in range(len(indkøb)):
        sorteret.append(indkøb[i].type + ", " + str(i))
    sorteret.sort()

    varer =[]
    indkøb_sorteret = []

    for i in range(len(indkøb)):
        print(sorteret[i][len(sorteret[i])-1])
        indkøb_sorteret.append(indkøb[int(sorteret[i][len(sorteret[i])-1])])
        varer.append(str(indkøb_sorteret[i].navn) + " x " +str(indkøb_sorteret[i].antal))
    indkøb = indkøb_sorteret

    seddel_listbox.delete(0, END)
    pris_listbox.delete(0, END)

    for i in range(len(indkøb)):
        seddel_listbox.insert(END, varer[i])
        pris_listbox.insert(END, float(indkøb[i].pris) * float(indkøb[i].antal))

def main():
    global valg, navn_entry, pris_entry, antal_entry, seddel_listbox, pris_listbox, indkøb, varer
    root = Tk()
    indkøb = []
    varer = []

    intro_text = 'Velkommen til indkøbsliste-skriveren. Dette program kan hjælpe dig med at skrive indkøbslister.\n Det smarte ved programmet er, at det kan sortere varerne efter hvilken afdeling, du ville finde dem i, i butikken\n For at starte skal du indtaste din første vare.\n Vælg hvilken afdeling varen kan findes i, og udfyld felterne til varens navn, pris og antal.\n Tryk derefter på "Tilføj vare"-knappen. Du kan også slette varer fra listen med "Slet vare"-knappen.\n Når du er færdig med at tilføje nye varer kan du sortere listen med "Sorter liste"-knappen'
    Label(root, text=intro_text).grid(column=1, row=1)

    mulighed_frame = Frame(root)
    mulighed_frame.grid(column=1, row=2, padx=5)

    afdeling_label = Label(mulighed_frame, text="Afdeling")
    afdeling_label.grid(column=1, row=1)


    afdelinger = ["Frugt og grønt", "Tøj", "Kosmetik", "Slagteri", "Frostvare", "Mejeri", "ØL og vand", "Snacks", "Morgenmadsprodukter", "Bageredskaber", "Andet" ]
    valg = StringVar()
    valg.set(afdelinger[0])
    afdelingsmenu = OptionMenu(mulighed_frame, valg, *afdelinger)
    afdelingsmenu.grid(column=1, row=2)

    navn_label = Label(mulighed_frame, text="Produktnavn")
    navn_label.grid(column=2, row=1)

    navn_entry = Entry(mulighed_frame)
    navn_entry.grid(column=2, row=2)

    pris_label = Label(mulighed_frame, text="Pris pr. stk.")
    pris_label.grid(column=3, row=1)

    pris_entry = Entry(mulighed_frame, width=10)
    pris_entry.grid(column=3, row=2)

    antal_label = Label(mulighed_frame, text="Antal")
    antal_label.grid(column=4, row=1)

    antal_entry = Entry(mulighed_frame, width=5)
    antal_entry.grid(column=4, row=2)

    box_frame = Frame(root)
    box_frame.grid(column=3, row=2, padx=15, pady=10)

    seddel_box_label = Label(box_frame, text="Indkøbsseddel")
    seddel_box_label.grid(column=1, row=1)

    seddel_listbox = Listbox(box_frame)
    seddel_listbox.grid(column=1, row=2)

    pris_box_label = Label(box_frame, text="Pris")
    pris_box_label.grid(column=2, row=1)

    pris_listbox = Listbox(box_frame, width=5)
    pris_listbox.grid(column=2, row=2)


    tilføj_knap = Button(mulighed_frame, text="Tilføj vare", command=tilføj, width=12)
    tilføj_knap.grid(column=5, row=1, pady=5, padx=10)

    slet_knap = Button(mulighed_frame, text="Slet vare", command=slet, width=12)
    slet_knap.grid(column=5, row=2, pady=5, padx=10)

    sorter_knap = Button(mulighed_frame, text="Sorter liste", command=sorter, width=12)
    sorter_knap.grid(column=5, row=3, pady=5, padx=10)

    root.mainloop()
main()